<?php 
namespace tech_test;

use tech_test\dataFormats\dataFormat;
use tech_test\challenges\challenge;

class renderer
{
    private $data_format;
    
    public function __construct(dataFormat $data_format)
    {
        $this->data_format = $data_format;
    }

    public function set($key, $value)
    {
        $this->data_format->set($key, $value);
    }

    public function setChallenge(challenge $challenge)
    {
        $this->data_format->set('challenge', $challenge->getBuild());
    }

    public function setMessage($message)
    {
        $this->data_format->set('message', $message);
    }

    public function render()
    {
        $this->data_format->send_headers();

        $output =  $this->data_format->generate();
        echo $output;
    }
}