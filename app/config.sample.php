<?php
/**
 * Config
 */

//define('DEBUG', true);  //For debugging database issues.

$config['challenge_complete_requirement'] = 50;

$config['messages'] = array(
    'challenge_completed' => 'Challenge completed',
    'invalid_challenge' => 'Invalid Challenge',
    'challenge_already_solved' => 'This challenge has already been solved. Start again.',
    'correct_answer' => 'Correct! - Next challenge.',
    'incorrect_answer' => 'Incorrect answer. Start again'
);

$config['host'] = 'localhost';

$config['db'] = array(
  'host' => 'localhost',
  'database' => '',
  'user' => '',
  'password' => ''
);