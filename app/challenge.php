<?php
namespace tech_test\challenges;

use tech_test\db;

class challenge
{
    private $challenge_id = null;
    private $sequence_id = 1;
    private $type = null;
    protected $arguments = array();
    private $answer = null;
    private $user_answer = null;
    private $result = false;
    private $source = '';

    public $debug = '';

    private $db;

    public function init(db $db, $data = null) {
        $this->db = $db;

        if(!is_null($data)) {
            $this->useData($data);
        }
    }

    public function useData($data) {
        $this->challenge_id = $data['challenge_id'];
        $this->sequence_id = $data['sequence'];
        $this->type = $data['type'];
        $this->arguments = unserialize($data['arguments']);
        $this->answer = $data['answer'];
        $this->user_answer = $data['user_answer'];
        $this->result = $data['result'];
        $this->source = $data['source'];

        $this->debug = 'Loaded from useData';
    }

    public function generateChallengeID() {
        $this->challenge_id = md5(uniqid('tech-test' . rand(), true));
    }

    public function getChallengeID() {
        return $this->challenge_id;
    }

    public function getSequenceID() {
        return $this->sequence_id;
    }

    public function setSequenceID($sequence_id) {
        $this->sequence_id = $sequence_id;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getArguments() {
        return $this->arguments;
    }

    public function setArguments($arguments) {
        $this->arguments = $arguments;
    }

    public function getAnswer() {
        return $this->answer;
    }
    
    public function setAnswer($answer) {
        $this->answer = $answer;
    }

    public function getUserAnswer() {
        return $this->user_answer;
    }

    public function setUserAnswer($answer) {
        $this->user_answer = $answer;

        $this->result = ($this->user_answer == $this->answer) ? true : false;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source)
    {
        $this->source = $source;
    }

    public function generate() {
        $this->generateChallengeID();
        $this->setSource($_SERVER['REMOTE_ADDR']);

        $this->sequence_id = 1;
    }

    public function saveChallenge() {
        $this->db->saveChallenge($this);
    }

    public function getBuild() {
        return array(
            'type' => $this->getType(),
            'arguments' => $this->getArguments()
        );
    }

    static function loadChallenge(db $db, $challenge_id) {
        $challenge = $db->getChallenge($challenge_id);

        return $challenge;
    }
}