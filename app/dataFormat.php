<?php
namespace tech_test\dataFormats;

class dataFormat
{
    protected $data;

    public function __construct($raw_data = null)
    {
        if(is_null($raw_data)) return false;
        $this->data = $this->convert($raw_data);
    }
    
    public function set($key, $value) {
        $this->data[$key] = $value;
    }
    
    public function get($key = null, $subkey = null) {
        if(is_null($key)) {
            return $this->data;
        }

        if(!is_null($key)) {
            if(!is_null($subkey)) {
                return @$this->data[$key][$subkey];
            }

            return @$this->data[$key];
        }
    }

    public function send_headers() {

    }

    public function generate() {

    }
}