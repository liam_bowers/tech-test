<?php
namespace tech_test\challenges;

class isAnagramChallenge extends challenge
{
    private $anagrams = array();

    public function generate()
    {
        parent::generate();

        //$this->testDictionary();

        $this->setType('is_anagram');

        $this->setArguments($this->generateArguments());

        $answer = (int) $this->generateAnswer();

        $this->setAnswer($answer);
    }

    private function generateArguments()
    {
        $this->loadAnagrams();

        if(rand(0, 1) == 1) {
            //Use real anagram
            $anagram = $this->anagrams[array_rand($this->anagrams, 1)];
            
        } else {
            //Use bad anagram
            $anagram = $this->generateBadAnagram();
        }

        return $anagram;
    }

    public function generateAnswer()
    {
        $arguments = $this->getArguments();

        return (int) $this->isAnagram($arguments[0], $arguments[1]);
    }

    private function isAnagram($string_a, $string_b) {
        $string_a = preg_replace('/[^a-z]/i', '', strtolower($string_a));
        $string_b = preg_replace('/[^a-z]/i', '', strtolower($string_b));

        $a_letters = str_split($string_a);
        sort($a_letters);

        $b_letters = str_split($string_b);
        sort($b_letters);

        if($a_letters == $b_letters) {
            return true;
        }

        return false;
    }

    private function loadAnagrams() {

        $anagrams = [];

        $file = new \SplFileObject(APP_PATH . 'data/anagrams.txt');
        $file->setFlags(\SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);

        // Loop until we reach the end of the file.
        while (!$file->eof()) {
            $line = trim($file->fgets());

            $anagrams[] = explode('|', $line, 2);
        }

        $this->anagrams = $anagrams;
    }

    private function generateBadAnagram() {
        $anagram = $this->anagrams[array_rand($this->anagrams, 1)];

        $random_id = rand(0, 1);
        $anagram[$random_id] = $this->injectCharacter($anagram[$random_id]);

        return $anagram;
    }

    private function injectCharacter($string) {
        $chars = 'abcdefghijklmnopqrstuvwxyxABCDEFGHIJKLMNOPQRSQTUVWXYZ';

        $random_position = rand(0, strlen($string)-1);
        $random_char = $chars[rand(0, strlen($chars)-1)];
        $string = substr($string, 0, $random_position) . $random_char . substr($string, $random_position);

        return $string;
    }

    private function testDictionary() {

        echo '<p>Anagram Dictionary test';

        $this->loadAnagrams();
        foreach($this->anagrams as $this_anagram) {

            if(count($this_anagram) != 2) {
                echo '<li>Not enough parts :' . implode('', $this_anagram);
                continue;
            }

            if(!$this->isAnagram($this_anagram[0], $this_anagram[1])) {
                echo '<li>Bad anagram: ' . implode(' + ', $this_anagram);
            }
        }

        die('<p>End');
    }
}