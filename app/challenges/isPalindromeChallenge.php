<?php
namespace tech_test\challenges;

class isPalindromeChallenge extends challenge
{
    private $palindromes = array();

    public function generate()
    {
        parent::generate();

        //$this->testDictionary();

        $this->setType('is_palindrome');

        $this->setArguments($this->generateArguments());

        $answer = (int) $this->generateAnswer();

        $this->setAnswer($answer);
    }

    private function generateArguments()
    {
        $this->loadPalindromes();

        if(rand(0, 1) == 1) {
            //Use real palindrome
            $palindrome = $this->palindromes[array_rand($this->palindromes, 1)];
            
        } else {
            //Use bad palindrome
            $palindrome = $this->generateBadPalindrome();
        }

        return $palindrome;
    }

    public function generateAnswer()
    {
        return (int) $this->isPalendrome($this->getArguments());
    }

    private function isPalendrome($string) {
        $string = preg_replace('/[^a-z]/i', '', strtolower($string));

        if($string == strrev($string)) {
            return true;
        }

        return false;
    }

    private function loadPalindromes() {

        $palindromes = [];

        $file = new \SplFileObject(APP_PATH . 'data/palindromes.txt');
        $file->setFlags(\SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);

        // Loop until we reach the end of the file.
        while (!$file->eof()) {
            $palindromes[] = trim($file->fgets());
        }

        $this->palindromes = $palindromes;
    }

    private function generateBadPalindrome() {
        $palindrome = $this->palindromes[array_rand($this->palindromes, 1)];

        $palindrome = $this->injectCharacter($palindrome);
        $palindrome = $this->injectCharacter($palindrome);  //If only one character is injected, it might be in the middle and still pass.

        return $palindrome;
    }

    private function injectCharacter($string) {
        $chars = 'abcdefghijklmnopqrstuvwxyxABCDEFGHIJKLMNOPQRSQTUVWXYZ';

        $random_position = rand(0, strlen($string)-1);
        $random_char = $chars[rand(0, strlen($chars)-1)];
        $string = substr($string, 0, $random_position) . $random_char . substr($string, $random_position);

        return $string;
    }

    private function testDictionary() {

        echo '<p>Palindrome Dictionary test';

        $this->loadPalindromes();
        foreach($this->palindromes as $this_palindrome) {
            if(!$this->isPalendrome($this_palindrome)) {
                echo '<li>Bad palindrome: ' . $this_palindrome;
            }
        }

        die('<p>End');
    }
}