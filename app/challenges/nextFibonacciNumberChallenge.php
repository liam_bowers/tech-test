<?php
namespace tech_test\challenges;

class nextFibonacciNumberChallenge extends challenge
{
    public function generate()
    {
        parent::generate();

        $this->setType('next_fibonacci_number');

        $this->setArguments($this->generateArguments());

        $answer = $this->generateAnswer();
        $this->setAnswer($answer);
    }

    private function generateArguments()
    {
        $argument_count = rand(1, 10);

        $arguments = array();
        for($i = 1; $i <= $argument_count; $i++) {
            $arguments[] = $this->getFibonacci($i);
        }

        return $arguments;
    }

    public function generateAnswer()
    {
        return $this->getFibonacci(count($this->getArguments()) + 1);

    }

    private function getFibonacci($n)
    {
        return round(pow((sqrt(5)+1)/2, $n) / sqrt(5));
    }
}