<?php
namespace tech_test\challenges;

class multiplicationChallenge extends challenge
{
    public function generate()
    {
        parent::generate();

        $this->setType('multiplication');

        $this->setArguments($this->generateArguments());

        $answer = $this->generateAnswer();
        $this->setAnswer($answer);
    }

    private function generateArguments()
    {
        $argument_count = rand(2, 5);

        $arguments = array();
        for($i = 1; $i <= $argument_count; $i++) {
            $arguments[] = rand(1, 12);
        }

        return $arguments;
    }

    public function generateAnswer()
    {
        return array_product($this->getArguments());

    }
}