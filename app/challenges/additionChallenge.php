<?php
namespace tech_test\challenges;

class additionChallenge extends challenge
{
    public function generate()
    {
        parent::generate();

        $this->setType('addition');

        $this->setArguments($this->generateArguments());

        $answer = $this->generateAnswer();
        $this->setAnswer($answer);
    }

    private function generateArguments()
    {
        $argument_count = rand(2, 5);

        $arguments = array();
        for($i = 1; $i <= $argument_count; $i++) {
            $arguments[] = rand(-250, 250);
        }

        return $arguments;
    }

    public function generateAnswer()
    {
        return array_sum($this->getArguments());

    }
}