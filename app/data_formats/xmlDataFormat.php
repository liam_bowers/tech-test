<?php
namespace tech_test\dataFormats;

class xmlDataFormat extends dataFormat
{
    public function send_headers() {
        header('Content-Type: text/xml');
    }

    public function generate() {
        $data = $this->get();

        $xml = new \SimpleXMLElement('<data />');

        $this->arrayToXML($data, $xml);

        $raw_xml = $xml->asXML();

        return $raw_xml;

    }
    
    public function convert($raw_data) {
        $xml = simplexml_load_string(utf8_encode($raw_data));

        $data = json_decode(json_encode((array) $xml), TRUE);

        return $data;
    }

    private function arrayToXML(&$data, &$xml) {

        $force_keys = ['arguments'];

        foreach($data as $key => $value) {
            if(is_array($value)) {
                if(in_array($key, $force_keys)) {
                    foreach($value as $this_value) {
                        $xml->addChild($key, $this_value);
                    }
                    continue;
                }

                $array_xml = $xml->addChild($key);
                $this->arrayToXML($value, $array_xml);
            } else {
                $xml->addChild($key, $value);
            }
        }

    }
}