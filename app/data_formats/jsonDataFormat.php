<?php
namespace tech_test\dataFormats;

class jsonDataFormat extends dataFormat
{
    public function send_headers() {
        header('Content-Type: application/json');
    }

    public function generate() {
        return json_encode($this->get());
    }
    
    public function convert($raw_data) {
        return json_decode($raw_data, true);
    }
}