<?php
namespace tech_test\dataFormats;

class yamlDataFormat extends dataFormat
{
    public function send_headers() {
        header('Content-Type: text/x-yaml');
    }

    public function generate() {
        return \Spyc::YAMLDump($this->get());
    }
    
    public function convert($raw_data) {
        return \Spyc::YAMLLoadString($raw_data);
    }
}