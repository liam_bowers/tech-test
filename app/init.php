<?php
if(!file_exists(APP_PATH . 'config.php')) {
    die('Config not found.');
}

include(APP_PATH . 'config.php');
include(APP_PATH . 'db.php');
include(APP_PATH . 'tech_test.php');
include(APP_PATH . 'challenge.php');

include(APP_PATH . 'challenges/additionChallenge.php');
include(APP_PATH . 'challenges/multiplicationChallenge.php');
include(APP_PATH . 'challenges/nextFibonacciNumberChallenge.php');
include(APP_PATH . 'challenges/isPalindromeChallenge.php');
include(APP_PATH . 'challenges/isAnagramChallenge.php');

include(APP_PATH . 'dataFormat.php');
include(APP_PATH . 'data_formats/jsonDataFormat.php');
include(APP_PATH . 'data_formats/xmlDataFormat.php');
include(APP_PATH . 'data_formats/yamlDataFormat.php');
include(APP_PATH . 'lib/Spyc.php');

include(APP_PATH . 'renderer.php');

//Solver
include(APP_PATH . 'solver.php');
include(APP_PATH . 'api.php');

ini_set('display_errors', defined('DEBUG'));