<?php
namespace tech_test\solver;

class api
{
    private $info = array();
    private $content_type = null;

    public function call($url, $parameters)
    {
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        $info = curl_getinfo($ch);

        //Get the content type
        $this->info = $info;
        $this->content_type = $this->ParseContentType($this->info['content_type']);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    public function get($url, $parameters)
    {
        return $this->call($url, $parameters);
    }

    public function ParseContentType($value)
    {
        $mime_parts = explode(';', $value);

        foreach($mime_parts as $part) {
            switch($part)
            {
                case 'text/x-yaml':
                case 'text/html':
                    return 'yaml';
                    break;

                case 'application/json':
                    return 'json';
                    break;

                case 'text/xml':
                    return 'xml';
                    break;
            }
        }

        trigger_error('Unable to find mime type from ' . $value, E_USER_ERROR);
    }

    public function getContentType()
    {
        return $this->content_type;
    }
}