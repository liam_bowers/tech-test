<?php

namespace tech_test\solver;

use tech_test\tech_test;

class solver {

    public function go() {
        global $config;

        $host = $config['host'];
        $url = $host . '';
        $parameters = array();
        $tech_test = new tech_test();
        $challenges = $tech_test->getChallenges();
        $data_formats = $tech_test->getDataFormats();

        $api = new api();
        $continue = 0;
        while($continue < 500) {
            echo '<li>**************************** [' . $continue . ']</li>' . PHP_EOL;
            $raw_result = $api->get($url, $parameters);

            echo '<li>Raw';
            var_dump($raw_result);

            $content_type = $api->getContentType();

            $data_format_class = 'tech_test\dataFormats\\' . $data_formats[$content_type];

            if (!class_exists($data_format_class)) {
                trigger_error('Unable to handle content type ' . $content_type, E_USER_ERROR);;
                die();
            }

            $extracted_data = new $data_format_class($raw_result);

            echo '<li>Extracted Data<pre>' . print_r($extracted_data->get(), true) . '</pre>';
            if($extracted_data->get('solved_challenge')) {
                exit('Success!');
            }

            //Figure out the problem
            $challenge_class = 'tech_test\challenges\\' . $challenges[$extracted_data->get('challenge', 'type')];

            if (!class_exists($challenge_class)) {
                trigger_error('Unable to handle problem ' . $extracted_data->get('challenge', 'type'), E_USER_ERROR);

                echo '<li>challenge: \'' . $extracted_data->get('challenge', 'type') . '\'';
                echo '<li>$challenges<pre>' . print_r($challenges, true) . '</pre>';
                die();
            }

            $challenge = new $challenge_class();
            $challenge->setArguments($extracted_data->get('challenge', 'arguments'));
            $answer = $challenge->generateAnswer();


            $url = $host . $extracted_data->get('endpoint') . urlencode($answer);

            $continue++;
        }
    }
}