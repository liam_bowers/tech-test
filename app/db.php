<?php
namespace tech_test;

use tech_test\challenges\challenge;

class db
{
    private $db = null;
    private $tables = array('challenges' => 'challenges');

    public function __construct($config)
    {
        $default_config = array(
          'host' => 'localhost',
          'database' => 'invalid_database',
          'user' => '',
          'password' => ''
        );

        $config = array_merge($default_config, $config);

        try {
            $db = new \PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['database'], $config['user'], $config['password']);
            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch(\PDOException $e) {
            if(defined('DEBUG')) {
                echo '<li>Error: ' . $e->getMessage();
            }

            die('<h1>System down for maintenance.</h1>');
        }

        $this->db = $db;
    }

    public function saveChallenge(challenge $challenge) {
        try {
            $query = $this->db->prepare('REPLACE INTO ' . $this->tables['challenges'] . ' 
        (challenge_id, sequence, type, arguments, answer, user_answer, result, time, source) 
        VALUES (:challenge_id, :sequence, :type, :arguments, :answer, :user_answer, :result, :time, :source)');

            $query->bindValue(':challenge_id', $challenge->getChallengeID());
            $query->bindValue(':sequence',   $challenge->getSequenceID());
            $query->bindValue(':type',       $challenge->getType());
            $query->bindValue(':arguments',  serialize($challenge->getArguments()));
            $query->bindValue(':answer',     $challenge->getAnswer());
            $query->bindValue(':user_answer',$challenge->getUserAnswer());
            $query->bindValue(':result',     $challenge->getResult());
            $query->bindValue(':time',       time());
            $query->bindValue(':source',     $challenge->getSource());

            $query->execute();

            //$affected_rows = $stmt->rowCount();
        } catch(\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function updateChallenge(challenge $challenge) {
        try {
            $query = $this->db->prepare('UPDATE ' . $this->tables['challenges'] . ' 
            SET user_answer = :user_answer, 
                result = :result
            WHERE challenge_id = :challenge_id
            LIMIT 1');

            $query->bindValue(':user_answer',   $challenge->getUserAnswer());
            $query->bindValue(':result', $challenge->getResult());

            $query->execute();

            //$affected_rows = $stmt->rowCount();
        } catch(\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getChallenge($challenge_id) {
        try {
            $query = $this->db->prepare('select * FROM ' . $this->tables['challenges'] . ' WHERE challenge_id = :challenge_id');

            $query->bindValue(':challenge_id', $challenge_id);

            $query->execute();

            $row = $query->fetch(\PDO::FETCH_ASSOC);

            return $row;

        } catch(\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return false;
    }
}