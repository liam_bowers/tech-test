<?php
namespace tech_test;


use tech_test\challenges\challenge;

class tech_test
{
    private $journey_id = null;
    private $challenges = array(
        'addition' => 'additionChallenge',
        'multiplication' => 'multiplicationChallenge',
        'next_fibonacci_number' => 'nextFibonacciNumberChallenge',
        'is_palindrome' => 'isPalindromeChallenge',
        'is_anagram' => 'isAnagramChallenge',
    );

    private $data_formats = array(
        'json' => 'jsonDataFormat',
        'xml' => 'xmlDataFormat',
        'yaml' => 'yamlDataFormat'
    );

    private $challenge_complete_requirement = 5;
    private $current_challenge = null;
    private $renderer = null;
    private $messages = array(
        'challenge_completed' => 'Challenge completed',
        'invalid_challenge' => 'Invalid Challenge',
        'challenge_already_solved' => 'This challenge has already been solved. Start again.',
        'correct_answer' => 'Correct! - Next challenge.',
        'incorrect_answer' => 'Incorrect answer. Start again'
    );

    private $database_config;

    public function __construct()
    {
        $data_format = $this->generateDataFormat();

        $this->renderer = new renderer($data_format);
    }
    
    public function useConfig($config) {
        if(isset($config['challenge_complete_requirement'])) {
            $this->setChallengeCompleteRequirement($config['challenge_complete_requirement']);
        }

        if(isset($config['messages'])) {
            foreach($config['messages'] as $key => $value) {
                $this->messages[$key] = $value;
            }
        }

        if(isset($config['db'])) {
            $this->database_config  = $config['db'];
        }
    }
    
    public function setChallengeCompleteRequirement($number) {
        $this->challenge_complete_requirement = $number;
    }

    private function getChallengeIDFromURL()
    {
        $request_uri = $_SERVER['REQUEST_URI'];

        if($request_uri == '/') return false;

        $script_path = dirname($_SERVER['SCRIPT_NAME']);

        $request_uri_parts = parse_url($_SERVER['REQUEST_URI']);

        $request_uri = substr($request_uri_parts['path'], strlen($script_path), strlen($request_uri_parts['path']) - strlen($script_path));
        $request_uri = trim($request_uri, '/');

        $url_parts = explode('/', $request_uri);
        $this->journey_id = array_shift($url_parts);

        return $this->journey_id;
    }

    //Challenges
    public function generateChallenge()
    {
        $random_challenge = $this->getRandomChallenge();

        $fq_challenge = 'tech_test\\challenges\\' . $this->challenges[$random_challenge];
        $challenge = new $fq_challenge();
        $challenge->init(new db($this->database_config));
        $challenge->generate();
        
        
        return $challenge;
    }

    public function getChallenges()
    {
        return $this->challenges;
    }

    private function getRandomChallenge()
    {
        return array_rand($this->challenges, 1);
    }
    
    //Output Formats
    public function generateDataFormat()
    {
        $random_data_format = $this->getRandomDataFormat();

        $fq_data_format = 'tech_test\\dataFormats\\' . $this->data_formats[$random_data_format];

        return new $fq_data_format;
    }

    public function getDataFormats()
    {
        return $this->data_formats;
    }

    private function getRandomDataFormat()
    {
        return array_rand($this->data_formats, 1);
    }

    //General challenge stuff

    public function hasSubmittedAnswer() {
        if(isset($_GET['answer'])) {
            return true;
        }

        return false;
    }
    
    public function loadChallenge($challenge_id = null) {
        $challenge_id = (!is_null($challenge_id)) ? $challenge_id : $this->getChallengeIDFromURL();
        
        $challenge_data = challenge::loadChallenge(new db($this->database_config), $challenge_id);

        if(!$challenge_data) return false;

        $fq_challenge = 'tech_test\\challenges\\' . $this->challenges[$challenge_data['type']];
        $challenge = new $fq_challenge();
        $challenge->init(new db($this->database_config), $challenge_data);
        $this->current_challenge = $challenge;
        return $challenge;
    }

    public function hasBeenAnswered() {
        $user_answer = $this->current_challenge->getUserAnswer();

        return !empty($user_answer) ? true : false;
    }

    public function setUserAnswer($answer) {
        $this->current_challenge->setUserAnswer($answer);
    }

    public function hasSolvedChallenge()
    {
        $this->current_challenge->saveChallenge();
        return $this->current_challenge->getResult();
    }

    public function hasSolvedAllChallenges()
    {
        if($this->current_challenge->getSequenceID() >= $this->challenge_complete_requirement) {
            return true;
        }

        return false;
    }

    public function createChallenge() {
        $challenge = $this->generateChallenge();
        $challenge->saveChallenge();

        $this->current_challenge = $challenge;
    }

    public function createNextChallenge() {

        $current_sequence_id = $this->current_challenge->getSequenceID();

        $challenge = $this->generateChallenge();
        $challenge->setSequenceID(++$current_sequence_id);
        $challenge->saveChallenge();

        $this->current_challenge = $challenge;
    }

    //Render

    public function render() {

        $this->renderer->setChallenge($this->current_challenge);
        $this->renderer->set('endpoint', '' . $this->current_challenge->getChallengeID() . '?answer=');
        $this->renderer->render();
    }
    
    public function renderSet($key, $value) {
        $this->renderer->set($key, $value);
    }

    public function renderChallengeCompleted() {

        $this->renderer->set('message', $this->messages['challenge_completed']);
        $this->renderer->set('solved_challenge', 1);
        $this->renderer->render();
    }

    public function go() {
        $solved_challenge = 0;

        if($this->hasSubmittedAnswer()) {
            //$tech_test->loadChallenge();
            if(!$this->loadChallenge()) {
                $this->renderSet('message', $this->messages['invalid_challenge']);
                $this->createChallenge();
            } elseif ($this->hasBeenAnswered()) {
                $this->renderSet('message', $this->messages['challenge_already_solved']);
                $this->createChallenge();
            } else {
                $this->setUserAnswer($_GET['answer']);

                if($this->hasSolvedChallenge()) {

                    if($this->hasSolvedAllChallenges()) {
                        //$tech_test->renderSet('message,', 'Congratulations, you win!');
                        $solved_challenge = 1;
                        $this->renderChallengeCompleted();
                        die();
                    } else {
                        $this->renderSet('message', $this->messages['correct_answer']);
                        $this->createNextChallenge();
                    }

                } else {
                    $this->renderSet('message', $this->messages['incorrect_answer']);
                    $this->createChallenge();
                }
            }

        } else {

            $this->createChallenge();
        }

        $this->renderSet('solved_challenge', $solved_challenge);
        $this->render();
    }
}