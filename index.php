<?php
/**
 * Created by PhpStorm.
 * User: liam.bowers
 * Date: 13/05/2016
 * Time: 15:35
 */

define('SITE_PATH', realpath(dirname(__FILE__)) . '/');
define('APP_PATH', SITE_PATH . 'app/');

require_once(APP_PATH . 'init.php');

$tech_test = new tech_test\tech_test();
$tech_test->useConfig($config);

$tech_test->go();