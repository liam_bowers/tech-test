<?php
/*
 * A curl script to solve the challenge.
 */
define('SITE_PATH', realpath('../') . '/');
define('APP_PATH', SITE_PATH . 'app/');

require_once(APP_PATH . 'init.php');

$solver = new \tech_test\solver\solver();

$solver->go();